import React, { useState } from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import ToggleUnitMeasure from "../ToggleUnitMeasure/ToggleUnitMeasure";

function SearchBar(props) {
  const [location, setLocation] = useState("");
  function submitLocation(locationInput) {
    // get value from the search bar, make it look nice
    // and pass it up the inheritance chain
    // and down to weather component where it will be used in an api call

    let comma = locationInput.trim().search(",");

    let city = props.capitalize(locationInput.substring(0, comma));
    let state = props.capitalize(locationInput.substring(comma + 1), "right");

    let styledLocation = [city, state];

    props.location(styledLocation);
  }

  return (
    <div className='input-group mb-3 offset-1 offset-lg-2 offset-md-2 offset-sm-1 col-lg-8 col-md-8 col-sm-10 col-10'>
      <input
        id='searchbar'
        type='text'
        className='form-control'
        placeholder='City, State/Country or zipcode'
        aria-label='Location Search Bar'
        onChange={(e) => setLocation(e.target.value)}
        required
        onKeyDown={(e) =>
          e.key === "Enter"
            ? location.length > 0
              ? submitLocation(location)
              : ""
            : ""
        }
      />
      <div className='input-group-append'>
        <button
          disabled={location.length <= 0}
          className='btn btn-primary'
          type='button'
          id='button-addon2'
          aria-label='Press enter or press this button to submit your location to the search bar'
          onClick={() =>
            location.length > 0
              ? submitLocation(location)
              : submitLocation("washington,dc")
          }
        >
          Search
        </button>
        <ToggleUnitMeasure getUnit={props.getUnit}></ToggleUnitMeasure>
      </div>
    </div>
  );
}

export default SearchBar;
