import React, { useState } from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";

//  This will contain a switch that will change the temperature display from farenheit to celsius and vis versa

function ToggleUnitMeasure(props) {
  const [unit, setUnit] = useState("C");

  function sendData() {
    unit === "F" ? setUnit("C") : setUnit("F");
    props.getUnit(unit);
  }

  return (
    <div>
      <button
        className='btn btn-warning'
        onClick={sendData}
        aria-label='Fahrenheit and Celsius toggle button, press to change the unit that the temperature will display as'
      >
        C / F
      </button>
    </div>
  );
}

export default ToggleUnitMeasure;
