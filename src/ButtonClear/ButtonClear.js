import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";

/*  Contains the button for clearing the results of the app
    Reset the app to have no data displayed, just default
*/

function ButtonClear(props) {
  return (
    <div>
      <button>Clear</button>
    </div>
  );
}

export default ButtonClear;
