Refer to : https://towardsdatascience.com/passing-data-between-react-components-parent-children-siblings-a64f89e24ecf

but how do you get info from a granchild up to the grandparent and then to an aunt component?
Callbacks
\<someComponent parentCallback = {props.callBackFunction}\>
but it's more like grandparentCallback because we don't call any functions that live in the parent component
names of functions and variables may have changed since I made this
I've done this twice,

1. with passing the location from the searchbar to Weather to call Mapquest API endpoint to get the coordinates of that location
2. just now with passing what unit we want the temperature to display as, lifting from the toggleUnitMeasure component all the way to App and down to Weather

App <- callbackfunction lives here, has a settingContainer compo with prop called parentCallback set to callbackfunction (the function that lives in App)
|
--SettingsContainer <- ToggleUnitMeasure component called here, takes a prop called parentCallback set to props.parentCallback
| |
| -- ToggleUnitMeasure <- button / unit change func / props.parentCallback(grandchildData) lives here / calls props.parentCallback(grandchildData)
--Weather
