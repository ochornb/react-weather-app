import React from "react";
import "../WeatherHourlyItem/WeatherHourlyItem.css";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

/*
<img
        className='hourly-icon'
        src={`https://openweathermap.org/img/wn/${props.icon}.png`}
        alt={`an icon of ${props.alt}`}
      ></img>
*/
function WeatherHourly(props) {
  return (
    <div className='card--content'>
      <p className='m-0'>{props.hour}</p>
      <div className='pt-2 pb-1'>
        <FontAwesomeIcon
          icon={props.getWeatherIcon(props.icon)}
          size='2x'
        ></FontAwesomeIcon>
      </div>
      <p>{props.temp === "Now" ? props.temp : props.temp + "°"}</p>
    </div>
  );
}

export default WeatherHourly;
