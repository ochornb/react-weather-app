import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import "./WeatherCurrentItem.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWind, faThermometerEmpty } from "@fortawesome/free-solid-svg-icons";

import { faSun } from "@fortawesome/free-regular-svg-icons";
function WeatherCurrentItem(props) {
  return (
    <div
      className='m-auto p-0 col-lg-6 col-md-6 col-sm-9 col-9 '
      id='todays-weather'
    >
      <div className='py-4'>
        <FontAwesomeIcon
          icon={props.getWeatherIcon(props.data.icon)}
          size='4x'
        ></FontAwesomeIcon>
      </div>

      <h3>{props.data.description}</h3>
      <p>{props.data.weekday}</p>
      <h1 className='mb-4'>{props.convert(props.data.temp)}°</h1>
      <div id='weather-details'>
        <div
          id='wind-details'
          className='weather-detail-item d-flex align-items-center justify-content-around text-left'
        >
          <FontAwesomeIcon icon={faWind} size='2x' />

          <p>
            WIND SPD<br></br>
            {props.data.wind} mi/hr
          </p>
        </div>
        <div
          id='pressure-details'
          className='weather-detail-item d-flex align-items-center justify-content-around text-left'
        >
          <span className='icon heartbeat'></span>

          <p>
            PRESSURE<br></br>
            {props.data.pressure} hPa
          </p>
        </div>
        <div
          id='feels-like-details'
          className='weather-detail-item d-flex align-items-center justify-content-around text-left'
        >
          <div className=''>
            <FontAwesomeIcon
              icon={faThermometerEmpty}
              size='2x'
            ></FontAwesomeIcon>
          </div>
          <div>
            <p>
              FEELS LIKE
              <br />
              {Math.ceil(props.convert(props.data.feelsLike))}°
            </p>
          </div>
        </div>
        <div
          id='uv-index-details'
          className='weather-detail-item d-flex align-items-center justify-content-around text-left'
        >
          <FontAwesomeIcon icon={faSun} size='2x'></FontAwesomeIcon>
          <p>
            UV INDEX <br></br>
            {props.data.uvIndex}
          </p>
        </div>
      </div>
    </div>
  );
}

export default WeatherCurrentItem;
