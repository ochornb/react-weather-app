import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import WeatherWeeklyItem from "../WeatherWeeklyItem/WeatherWeeklyItem";

function WeatherWeeklyContainer(props) {
  let weeklyForcast = props.data.map((item) => {
    return (
      <div className='offset-1 offset-lg-2 col-lg-4 col-md-8 col-sm-7 col-9'>
        <WeatherWeeklyItem
          key={item.key}
          day={new Intl.DateTimeFormat("en-US", {
            weekday: "long",
          }).format(item.weekday * 1000)}
          weatherIcon={item.icon}
          //Change temp conversion over here
          low={props.convert(item.low)}
          high={props.convert(item.high)}
          alt={item.alt}
          getWeatherIcon={props.getWeatherIcon}
        ></WeatherWeeklyItem>
      </div>
    );
  });
  return weeklyForcast.slice(1, weeklyForcast.length - 1); // remove the first weather item, it's displayed as "current" on top of everything else; remove last one to make the display look nicer
}

export default WeatherWeeklyContainer;
