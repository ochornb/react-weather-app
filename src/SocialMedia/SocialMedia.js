import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import { FontAwesomeIcon } from "../../node_modules/@fortawesome/react-fontawesome";
//import { faCoffee } from "@fortawesome/free-solid-svg-icons";
function SocialMedia(props) {
  return (
    <div>
      <a href={props.link}>
        <FontAwesomeIcon icon={props.icon} />
      </a>
    </div>
  );
}

export default SocialMedia;
