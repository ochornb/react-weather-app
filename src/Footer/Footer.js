import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import { FontAwesomeIcon } from "../../node_modules/@fortawesome/react-fontawesome";
import {
  faLinkedin,
  faBitbucket,
  faCodepen,
} from "../../node_modules/@fortawesome/free-brands-svg-icons";
import { faLink } from "../../node_modules/@fortawesome/free-solid-svg-icons";

//  This shall contain icons and links to various social medias
function Footer() {
  return (
    <div>
      <div className='mb-3 d-flex justify-content-center'>
        <a
          href='https://www.linkedin.com/in/oliviahornback/'
          className='btn btn-light'
          aria-label='external link to LinkedIn page'
        >
          <FontAwesomeIcon icon={faLinkedin} size='5x' />
        </a>

        <a
          href='https://bitbucket.org/ochornb/react-weather-app/src/master/'
          className='btn btn-light'
          aria-label='external link to developer repository'
        >
          <FontAwesomeIcon icon={faBitbucket} size='5x' />
        </a>
        <a
          href='https://ochornb.bitbucket.io/'
          className='btn btn-light'
          aria-label='External link to web developer portfolio'
        >
          <FontAwesomeIcon icon={faLink} size='5x' />
        </a>
      </div>
      <p className='text-secondary'>
        Weather App Design by{" "}
        <a
          href='https://dribbble.com/shots/14328625-Weather-Forecast-App'
          target='_blank'
        >
          Muhammad Noufal
        </a>
      </p>
    </div>
  );
}

export default Footer;
