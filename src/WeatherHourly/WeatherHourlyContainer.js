import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import WeatherHourlyItem from "../WeatherHourlyItem/WeatherHourlyItem.js";
function WeatherHourlyContainer(props) {
  let currentDate = new Date();
  let currentHour = currentDate.getHours();
  let currentDay = currentDate.getDay();
  let hourlyForcast = props.data.map((item) => {
    // use the hours from the hourly forcasts date time string
    // to compare to our current hour
    // we want the current hour to display "Now"
    // and not next day's hour to display "now" i.e. 11 am today vs 11 am tomorrow, display temperature for that
    let forcastDate = new Date(item.hour * 1000);
    let hour = forcastDate.getHours();
    let day = forcastDate.getDay();

    return (
      <WeatherHourlyItem
        key={item.key}
        hour={
          hour === currentHour
            ? day === currentDay
              ? "Now"
              : `${hour}:00`
            : `${hour}:00`
        }
        icon={item.icon}
        temp={props.convert(item.temp)}
        alt={item.alt}
        getWeatherIcon={props.getWeatherIcon}
      ></WeatherHourlyItem>
    );
  });

  return hourlyForcast;
}

export default WeatherHourlyContainer;
