import React, { useState } from "react";
import "./App.css";
import Footer from "../Footer/Footer";
import Weather from "../Weather/Weather";
import SearchBarContainer from "../SearchBarContainer/SearchBarContainer";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";

function App() {
  const [location_app, setLocation_app] = useState(["Washington", "DC"]);
  const [unit_app, setUnit_app] = useState("");
  function updateLocation(location) {
    setLocation_app(location);
  }

  function getUnit(childData) {
    // pass this function down the the children chain,
    // down to toggleUnitMeasure component
    // call it in toggleUnitMeasure component
    // and pass the information back up the inheritance chain
    // and set the data passed in from toggleUnitMeasure
    // copy this idea when you want a child component to pass information to its aunts/uncles or cousins
    setUnit_app(childData);
  }

  const setAnimation = (element, animation, isVisible, prefix = "animate__") =>
    // we create a promise and return it
    new Promise((resolve, reject) => {
      const animationName = `${prefix}${animation}`;
      const node = document.getElementById(element);

      node.classList.add(`${prefix}animated`, animationName);
      node.style.visibility = "visible";

      //when the animation ends, we clean the classes and resolve the Promise
      function handleAnimationEnd(event) {
        event.stopPropagation();
        node.classList.remove(`${prefix}animated`, animationName);
        isVisible
          ? (node.style.visibility = "visible")
          : (node.style.visibility = "hidden");
        resolve("Animation ended");
      }
      node.addEventListener("animationend", handleAnimationEnd, { once: true });
    });

  //check it for syntax washington, D.C or zip code, clean inputs

  //return it
  //send it to weather.js
  //string.trim(); to get rid of extra whitespace
  //regex want the first half of the location to be Captialized i.e. Louisville, KY
  // regex want the second half (conditional)
  //       If the 2nd half has 3 or less characters... capitalize all
  //       otherwise
  //       capitalize the first letter
  //       i.e. Louisville, KY vs Louisville, Kentucky vs Louisville, KY 40515 vs Louisville, Kentucky 40515

  function capitalize(word, side = "left") {
    word = word.trim().toLowerCase();
    word = word.substring(0, 1).toUpperCase() + word.substring(1);
    let space;
    if (side === "right") {
      space = word.search(" ");
      if (space !== -1) {
        return `${capitalize(word.substring(0, space), "right")} ${capitalize(
          word.substring(space, word.length),
          "right"
        )}`;
      } else {
        if (word.length <= 3) {
          return word.toUpperCase();
        } else {
          return word;
        }
      }
    } else if (side === "left") {
      space = word.search(" ");
      if (space !== -1) {
        return `${capitalize(word.substring(0, space), "left")} ${capitalize(
          word.substring(space),
          "left"
        )}`;
      } else {
        return word;
      }
    }
  }
  return (
    <div className='App container p-0'>
      <div id='app-inner-container'>
        <SearchBarContainer
          location={updateLocation}
          getUnit={getUnit}
          setAnimation={setAnimation}
          capitalize={capitalize}
        />
        <Weather
          location={location_app}
          unit={unit_app}
          setAnimation={setAnimation}
          capitalize={capitalize}
        />
        <Footer />
      </div>
    </div>
  );
}

export default App;
