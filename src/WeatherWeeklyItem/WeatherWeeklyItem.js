import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import "./WeatherWeeklyItem.css";

function WeatherWeeklyItem(props) {
  return (
    <div className='row d-flex justify-content-between align-items-center py-2'>
      <p id={props.day} className='day'>
        {props.day}
      </p>

      <div className='weather-icon'>
        <FontAwesomeIcon
          icon={props.getWeatherIcon(props.weatherIcon)}
          size='2x'
        ></FontAwesomeIcon>
      </div>
      <p className='temperature'>
        <span className='temp-low'>{props.low}</span> /{" "}
        <span className='temp-high'>{props.high}°</span>
      </p>
    </div>
  );
}

export default WeatherWeeklyItem;
