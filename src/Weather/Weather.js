/* 
    This will contain several weather item components
    There will be a large one that will be Today's weather
    followed by 4 smaller components showing the rest of the week's weather
*/
import React, { useEffect, useState } from "react";

import WeatherCurrentItem from "../WeatherCurrentItem/WeatherCurrentItem";
import WeatherHourlyContainer from "../WeatherHourly/WeatherHourlyContainer";
import WeatherWeeklyContainer from "../WeatherWeeklyContainer/WeatherWeeklyContainer";

import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import "./Weather.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";
import getWeatherIcon from "../getWeatherIcon";
const API_KEY = process.env.REACT_APP_API_KEY;
const GEO_ENCODER_KEY = process.env.REACT_APP_GEO_ENCODER_KEY;

function Weather(props) {
  // get the location name "seattle, washington" from the search bar component (which has been traversed up to the App grandparent-component)
  // get city, state from user from search bar, in settings container
  let location = ["washington", "dc"];
  location = props.location;

  function convertFtoC(temperature) {
    return props.unit === "F" || props.unit === ""
      ? Math.round(temperature)
      : Math.round((temperature - 32) / 1.8);
  }

  // save information we get from openweathermap api
  // Why?
  // If we just save the response to a variable, we can't traverse down tempVar.data.weather[0]... gives error
  // so at the time of fetch, populate hooks with our needed data so we can use it outside of the fetch call
  // populating some default data from an OpenWeatherMap One Call
  const [fetchApiData, setFetchApiData] = useState({
    current: {
      weekday: "Monday",
      icon: "04n",
      temp: 23.23,
      description: "Cloudy",
      wind: 3,
      pressure: 1029,
      uvIndex: 1.39,
      feelsLike: 41,
    },
    weekly: {
      arr: [
        {
          key: 1614704400,
          weekday: 1614704400,
          icon: "01d",
          low: 29.16,
          high: 43.21,
          alt: "clear sky",
        },
        {
          key: 1614790800,
          weekday: 1614790800,
          icon: "01d",
          low: 33,
          high: 55,
          alt: "clear sky",
        },
        {
          key: 1614790801,
          weekday: 1614790801,
          icon: "01d",
          low: 33,
          high: 55,
          alt: "clear sky",
        },
      ],
    },
    hourly: {
      arr: [
        {
          key: 1614369600,
          hour: 1614369600,
          icon: "01d",
          temp: 49.23,
          alt: "sunny sky",
        },
        {
          key: 1614376800,
          hour: 1614376800,
          icon: "04d",
          temp: 47,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
        {
          key: 1614380400,
          hour: 1614380400,
          icon: "04n",
          temp: 46,
          alt: "overcast clouds",
        },
      ],
    },
  });
  const [hourly, setHourly] = useState([]);
  const [weekly, setWeekly] = useState([]);
  const [currentWeather, setCurrentWeather] = useState([]);
  const [coordinates, setCoordinates] = useState({
    latitude: 38.892062,
    longitude: -77.019912,
  });

  useEffect(() => {
    const getCoordinates = async () => {
      await fetch(
        `https://www.mapquestapi.com/geocoding/v1/address?key=${GEO_ENCODER_KEY}&location=${location[0]},${location[1]}`,
        { method: "GET" }
      )
        .then((res) => res.json())
        .then((data) => {
          let a = data.results[0].locations[0].latLng.lat;
          let b = data.results[0].locations[0].latLng.lng;
          setCoordinates({ latitude: a, longitude: b });
        })
        .catch((error) => {
          console.log(error);
        });
    };
    getCoordinates();
  }, [location, coordinates.latitude, coordinates.longitude]);

  // get the weather
  useEffect(() => {
    const fetchData = async () => {
      fetchApiData.hourly.arr = [];
      fetchApiData.weekly.arr = [];
      await fetch(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${coordinates.latitude}&lon=${coordinates.longitude}&exclude=minutely&units=imperial&appid=${API_KEY}`,
        { method: "POST" }
      )
        .then((res) => res.json())
        .then((data) => {
          setFetchApiData({
            current: {
              weekday: new Intl.DateTimeFormat("en-US", {
                weekday: "long",
              }).format(new Date(data.current.dt * 1000)),
              icon: data.current.weather[0].icon,
              alt: data.current.weather[0].description,
              temp: Math.round(data.current.temp),
              description: props.capitalize(
                data.current.weather[0].description
              ),
              wind: data.current.wind_speed,
              pressure: data.current.pressure,
              feelsLike: data.current.feels_like,
              uvIndex: data.current.uvi,
            },
            weekly: {
              //map data
              arr: data.daily.map((forcast) => {
                return {
                  key: forcast.dt,
                  weekday: forcast.dt,
                  icon: forcast.weather[0].icon,
                  low: forcast.temp.min,
                  high: forcast.temp.max,
                  alt: forcast.weather[0].description,
                };
              }),
            },
            // since open call api gives us an hourly forcast array of 48 hours
            // and we just want now + 24 hours
            // let's splice the array
            hourly: {
              arr: data.hourly.splice(0, 25).map((forcast) => {
                return {
                  key: forcast.dt,
                  hour: forcast.dt,
                  icon: forcast.weather[0].icon,
                  temp: forcast.temp,
                  alt: forcast.weather[0].description,
                };
              }),
            },
          });
        })
        .catch((error) => console.log(error));
    };
    fetchData();
  }, [coordinates.latitude, coordinates.longitude]);

  useEffect(() => {
    setCurrentWeather(
      <WeatherCurrentItem
        data={fetchApiData.current}
        convert={convertFtoC}
        getWeatherIcon={getWeatherIcon}
      />
    );
    setHourly(
      <WeatherHourlyContainer
        data={fetchApiData.hourly.arr}
        convert={convertFtoC}
        getWeatherIcon={getWeatherIcon}
      />
    );

    setWeekly(
      <WeatherWeeklyContainer
        data={fetchApiData.weekly.arr}
        convert={convertFtoC}
        getWeatherIcon={getWeatherIcon}
      />
    );
  }, [fetchApiData, props.unit]);

  return (
    <div id='weather'>
      <div className='row'>
        <div className='col offset-lg-3 offset-md-3 offset-sm-2 offset-2 pl-lg-1 pl-md-1 pl-0'>
          <h4 className='text-left '>
            <span className='location-city'>{props.location[0]}</span>
            {", "}
            <span className='location-state'>{props.location[1]}</span>
          </h4>
        </div>
      </div>

      {currentWeather}

      <div className='col-lg-6 col-md-6 col-sm-9 col-9 m-auto p-0'>
        <div className='d-flex justify-content-between pt-3'>
          <p className=''>
            <b>Today</b>
          </p>
          <p
            onClick={() => {
              let id = "weekly-weather-screen";
              props.setAnimation(id, "slideInRight", true);
              document.getElementById(id).style.right = "0%";
            }}
            className='text-right text-secondary word-btn'
          >
            <b>
              Next 7 Days{" "}
              <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
            </b>
          </p>
        </div>
        <div
          className='card d-flex flex-row mb-4'
          title='Press and hold shift while scrolling with mouse wheel'
        >
          {hourly}
        </div>
      </div>

      <div id='weekly-weather-screen'>
        <div className='d-flex mt-3 offset-lg-2 offset-md-1 offset-sm-1 offset-0'>
          <span
            className='col-2 word-btn'
            onClick={() => {
              props.setAnimation(
                "weekly-weather-screen",
                "slideOutRight",
                false
              );
            }}
          >
            <FontAwesomeIcon icon={faChevronLeft} size='2x'></FontAwesomeIcon>
          </span>
          <p className='col-lg-6 col-md-8 col-sm-7 col-9'>
            <span className='location-city'>{props.location[0]}</span>,{" "}
            <span className='location-state'>{props.location[1]}</span>
          </p>
        </div>
        <div className='offset-lg-3 offset-md-2 offset-sm-2 offset-1'>
          <h4 className='text-left font-weight-bold pl-3'>Next 7 Days</h4>
          {weekly}
        </div>
      </div>
    </div>
  );
}

export default Weather;
