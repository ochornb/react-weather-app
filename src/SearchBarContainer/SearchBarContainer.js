import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import SearchBar from "../SearchBar/SearchBar";
import "./SearchBarContainer.css";
import "animate.css";
/*  This will host the searchbar
    This will be hidden by a hamburger menu button
    parentcallback is used for the ToggleUnitMeasure component that resides in the searchbar component
    when the button is pressed, it will trigger the callback func  
    that travels up the parent/grandparent chain to "App" and down to the "Weather" component
    and converts temperatures in the Weather component to C or F
*/

function SearchBarContainer(props) {
  function toggleSettings() {
    const settings = "search-bar-container";
    const hamburger = document.getElementById("hamburger");
    if (hamburger.checked) {
      props.setAnimation(settings, "fadeInDown", true);
    } else {
      props.setAnimation(settings, "fadeOutUp", false);
    }
  }

  return (
    <div className=''>
      <div className='d-flex justify-content-end col-sm-10 col-11 offset-1'>
        <input type='checkbox' id='hamburger' onClick={toggleSettings}></input>
        <label
          id='hamburger_icon'
          htmlFor='hamburger'
          aria-label='Open and close the search box'
        >
          <span></span>
          <span></span>
          <span></span>
        </label>
      </div>

      <div id='search-bar-container'>
        <div>
          <SearchBar
            location={props.location}
            getUnit={props.getUnit}
            capitalize={props.capitalize}
          />
        </div>
      </div>
    </div>
  );
}

export default SearchBarContainer;
