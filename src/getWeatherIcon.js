// hot-linking is bad! Let's not use up open weather map's data anymore than we have to
// Let's use fontawesome icons in place of OWM's
// create an array of objects that has an id of what OWM can return
// with an icon value of a fontawesome icon name
import React from "react";
import {
  faCloudMoonRain,
  faCloudSun,
  faCloudSunRain,
  faSun,
  faMoon,
  faBolt,
  faCloud,
  faSnowflake,
  faSmog,
  faCloudMoon,
} from "@fortawesome/free-solid-svg-icons";

let WeatherIcons = [
  { id: "01d", icon: faSun },
  { id: "01n", icon: faMoon },
  { id: "02d", icon: faCloudSun },
  { id: "02n", icon: faCloudMoon },
  { id: "03d", icon: faCloud },
  { id: "03n", icon: faCloud },
  { id: "04d", icon: faCloud },
  { id: "04n", icon: faCloud },
  { id: "10d", icon: faCloudSunRain },
  { id: "10n", icon: faCloudMoonRain },
  { id: "11d", icon: faBolt },
  { id: "11n", icon: faBolt },
  { id: "13d", icon: faSnowflake },
  { id: "13n", icon: faSnowflake },
  { id: "50d", icon: faSmog },
];

function getWeatherIcon(id) {
  return WeatherIcons.find((icon) => icon.id === id).icon;
}

export default getWeatherIcon;
