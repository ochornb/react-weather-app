This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Weather App Made with React

---

## View it [here](https://react-weather-app-och.herokuapp.com/)

---

[![Image of Weather App](https://bitbucket.org/ochornb/ochornb.bitbucket.io/raw/4372da36b93b8ac014dc4446d7067a02e1b1a071/portfolio/imgs/react-weather-app.png)](https://react-weather-app-och.herokuapp.com/)

## Tools used

---

- HTML, CSS, JavaScript
- Bootstrap
- Git / Bitbucket
- ReactJS

## Summary

---

I made this project in order to better understand React function components and useState hooks.
By using Hooks, I make my code shorter and cleaner without having to call ComponentDidMount() and similar methods.

## Problems faced and solutions for them

---

**Environment Variables:**
This is what I had trouble with when making my first Weather App.
In that app, I didn't know how to host my apps to websites like Heroku but I knew
that saving an API key in the app itself or saving a git commit that contained the API key
was a bad idea. In that app, I made sure that the API key was not stored in the code and made
an extra form input to accept an API key in order to run the app.

This time, I learned how to use .env variables + git ignore to make sure API keys were
not exposed during development. Then I learned how to host this app using Heroku
and saving the API keys to the config vars!
